# BSGrow
look grow on [grow.epam.com](https://grow.epam.com/pdp/43334)

### Task on the topic "Code Conventions"
 - branch `crazy` - start playground project (demonstration of incorrect code with violations)
 - branch `normal` - project with code refaсtoring (in accordance with code сonventions)

### Tasks on the topic "iOS App Life Cycle" [branch `master`]
- "AppLifeCycle" - demo with app life cycle
- [in Dev] "BackgroundApp" - demo with background executing
- [in Dev] "CameraDemo" - demo with camera & CoreMotion
