//
//  SecondViewController.swift
//  AppLifeCycle
//
//  Created by Sergey Bizunov on 28.07.17.
//  Copyright © 2017 Sergey Bizunov. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    var textForLabel: String?

    @IBOutlet weak var label: UILabel!


    //#1
    override func loadView() {
        super.loadView()
        // Переопределить этот метод, чтобы вручную создавать свои представления
        print("=== VC2 loadView() ===")
    }
    
    //#2
    override func viewDidLoad() {
        super.viewDidLoad()
        // Представление создается и загружается в память
        print("=== VC2 viewDidLoad() ===")
    }
    
    //#3
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // На экране появляется представление
        print("=== VC2 viewWillAppear(\(animated)) ===")
    }
    
    //#4
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        // Представление предназначено для компоновки своих подвидов
        print("=== VC2 viewWillLayoutSubviews() ===")
    }
    
    //#5
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // Подвиды были настроены
        print("=== VC2 viewDidLayoutSubviews() ===")
    }
    
    //#6
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("=== VC2 viewDidAppear(\(animated)) ===")
        // Представления отображены на экране
        
        label.text = textForLabel
    }
    
    //++
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Подготовка к исчезновению ViewController-а
        print("=== VC2 viewWillDisappear(\(animated)) ===")
    }
    
    //++
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // Исчезновение ViewController-а
        print("=== VC2 viewDidDisappear(\(animated)) ===")
    }
}
