//
//  ViewController.swift
//  AppLifeCycle
//
//  Created by Sergey Bizunov on 28.07.17.
//  Copyright © 2017 Sergey Bizunov. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    @IBOutlet weak var outlet: UITextField!
    
    // Передача значения текстового поля
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if outlet.text == "" { (segue.destination as? SecondViewController)?.textForLabel = "Nothing 😀"
        } else { (segue.destination as? SecondViewController)?.textForLabel = outlet.text! }
    }
    
    //#1
    override func loadView() {
        super.loadView()
        // Вызов когда контроллер вида создается программно
        print("=== VC1 loadView() ===")
    }
    
    //#2
    override func viewDidLoad() {
        super.viewDidLoad()
        // Представление создается и загружается в память
        print("=== VC1 viewDidLoad() ===")
    }
    
    //#3
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // На экране появляется представление
        print("=== VC1 viewWillAppear(\(animated)) ===")
    }
    
    //#4
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        // Представление предназначено для компоновки своих подвидов
        print("=== VC1 viewWillLayoutSubviews() ===")
    }
    
    //#5
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // Подвиды были настроены
        print("=== VC1 viewDidLayoutSubviews() ===")
    }
    
    //#6
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Представления отображены на экране
        print("=== VC1 viewDidAppear(\(animated)) ===")
    }
    
    //++
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Подготовка к исчезновению ViewController-а
        print("=== VC1 viewWillDisappear(\(animated)) ===")
    }
    
    //++
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // Исчезновение ViewController-а
        print("=== VC1 viewDidDisappear(\(animated)) ===")
    }
}

